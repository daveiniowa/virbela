﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApi.Entities;
using WebApi.Models;
using WebApi.Services;

namespace virbelacodingtestweb.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ListingController : ControllerBase
    {

        private IListingService _listingService;

        public ListingController(IListingService listingService)
        {
            _listingService = listingService;
        }

        [Authorize]
        [HttpPost("Listing")]
        public IActionResult AddListing(ListingRequest model)
        {
            var user = (User)HttpContext.Items["User"];
            var response = _listingService.AddListing(model, user.Id);

            if (response == null)
                return BadRequest(new { message = "Failed to create listing" });

            return Ok(response);
        }

        [Authorize]
        [HttpGet("Listing")]
        public IActionResult GetListing(int listingId)
        {
            var response = _listingService.GetListingById(listingId);

            if (response == null)
                return BadRequest(new { message = "Failed to create listing" });

            return Ok(response);
        }

        [Authorize]
        [HttpGet("Listings")]
        public IActionResult GetAllListings()
        {
            var response = _listingService.GetAllListings();

            return Ok(response);
        }

        [Authorize]
        [HttpDelete("Listing")]
        public IActionResult RemoveListing(int listingId)
        {
            if (!_listingService.HasUpdatePermissions(listingId, (User)HttpContext.Items["User"]))
            {
                return new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
            }
            else
            {
                return Ok(_listingService.DeleteListing(listingId));
            }
        }


        [Authorize]
        [HttpPatch("Listing")]
        public IActionResult UpdateListing(ListingRequest updateRequest)
        {
            if (!_listingService.HasUpdatePermissions(updateRequest.Key, (User)HttpContext.Items["User"]))
            {
                return new JsonResult(new { message = "Unauthorized" }) { StatusCode = StatusCodes.Status401Unauthorized };
            }
            else
            {
                return Ok(_listingService.UpdateListing(updateRequest));
            }
        }

    }
}
