﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using WebApi.Entities;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            var response = _userService.Authenticate(model);

            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(response);
        }

        /// <summary>
        /// This allows for the creation of a new user to authenticate within the system
        /// </summary>
        /// <param name="model">Request object containing user credentials</param>
        /// <returns>User Information</returns>
        [HttpPost("register")]

        public IActionResult Register(AuthenticateRequest model)
        {
            var response = _userService.AddUser(model);

            if (response == null)
                return BadRequest(new { message = "Failed to add" });

            return Ok(response);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }
    }
}
