﻿using System;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Entities
{
    public class ListingContext : DbContext
    {
        public DbSet<Listing> Listings { get; set; }
        public DbSet<User> Users { get; set; }

        public string DbPath { get; private set; }

        public ListingContext(DbContextOptions contextOptions)
        : base(contextOptions)
        {
            var folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            DbPath = $"{path}{System.IO.Path.DirectorySeparatorChar}Virbela.db";
        }

        // The following configures EF to create a Sqlite database file in the
        // special "local" folder for your platform.
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source={DbPath}");
    }
}
