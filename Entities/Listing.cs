using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Entities
{
    public class Listing
    {
        [Key]
        public int Key { get; set; }
        public String Title { get; set; }

        public String Description { get; set; }

        public double Price { get; set; }

        [ForeignKey("AddedUser")]
        public int UserId { get; set; }

        User AddedUser { get; set; }

    }
}
