using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class ListingRequest
    {
        [Required]
        public int Key { get; set; }
        [Required]
        public String Title { get; set; }
        [Required]
        public String Description { get; set; }
        [Required]
        public double Price { get; set; }
    }
}