﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models;

namespace WebApi.Services
{
    public interface IListingService
    {
        Listing GetListingById(int id);
        Listing AddListing(ListingRequest listing, int userId);
        List<Listing> GetAllListings();
        bool DeleteListing(int listingId);
        Listing UpdateListing(ListingRequest listing);
        bool HasUpdatePermissions(int listingId, User user);
    }

    public class ListingService : IListingService
    {
        private readonly AppSettings _appSettings;
        private readonly ListingContext _context;
        public ListingService(IOptions<AppSettings> appSettings, ListingContext context)
        {
            _appSettings = appSettings.Value;
            _context = context;
        }

        public Listing GetListingById(int id)
        {
            return _context.Listings.Where(x => x.Key == id).FirstOrDefault();
        }

        public Listing AddListing(ListingRequest listing, int userId)
        {
            Listing newlisting = new Listing
            {
                Description = listing.Description,
                Price = listing.Price,
                Title = listing.Title,
                UserId = userId
            };
            _context.Add(newlisting);
            _context.SaveChanges();
            return newlisting;
        }

        public List<Listing> GetAllListings()
        {
            return _context.Listings.ToList() ;
        }

        public bool DeleteListing(int listingId)
        {
            var listing = GetListingById(listingId);
            _context.Listings.Remove(listing);
            return _context.SaveChanges() > 0;
        }

        public Listing UpdateListing(ListingRequest listing)
        {
            var existingListing = GetListingById(listing.Key);

            existingListing.Price = listing.Price;
            existingListing.Title = listing.Title;
            existingListing.Description = listing.Description;

            _context.SaveChanges();

            return existingListing;
        }

        public bool HasUpdatePermissions(int listingId, User user)
        {
            var listing = _context.Listings.Where(x => x.Key == listingId).FirstOrDefault();
            return user.Id == listing.UserId;
        }
    }
}