# Virbela Coding Test #

Welcome. We've created this test to gain insights into your development skills. Please follow the instructions below.

### Test overview ###

In this test you'll be asked to complete one or more exercises. When completed you'll push this repo to a public repository for evaluation. After evaluation, your contact at Virbela will reach out to you with next steps.

Feel free to use whatever resources you'd typically use when doing development work at a job. Internet searches, books, etc. are all acceptable sources. Your friend, internet coder for hire, or an already completed test are not.

### Instructions ###

* Clone this repository to your development machine.
* Navigate to the exercise you've been instructed to complete for detailed instructions.

**Once you have completed your exercise:**

* Commit and push the entire repository, with your completed project, back into a repository host of your choice (bitbucket, github, gitlab, etc)
* Share your project URL with your Virbela contact (Recruiter or Hiring Manager)

### Exercises ###

* [Exercise 1](./Exercise1/EXERCISE_1.md)
* [Exercise 2](./Exercise2/EXERCISE_2.md)

### If you have questions ###

* Reach out to your Virbela contact (Recruiter or Hiring Manager)

### Completed project for David M
* This project utilizes SqlLite and EntityFramework code first to build out the local database store.
* The migration is already created.
* Within the terminal please `dotnet ef database update` to generate your local database.
* After the database is created the project can be started and run with localhost and self documented swagger api
* https://localhost:5001/swagger/index.html will show the list of the routes available
* new users can be created using register
* log in using the register which will return a token
* at the top of the swagger page is a lock, click that and provide token from previous step to run authentication

## Questions ##

 1. **How can your implementation be optimized?** The implementation should hash passwords before it would be used for secured passwords. Unit testing and splitting the solution into unique projects would also help.
 1. **How much time did you spend on your implementation?** 3-4 hours.
 1. **What was most challenging for you?** This was the first solution I had worked on with using JWT tokens. I leveraged a tutorial and their example to get a jump start with the pattern. That component was the most challenging for me but I enjoyed seeing it function.
